/*
 * @Author: sky
 * @Date: 2023-09-13 00:17:51
 * @LastEditors: sky
 * @LastEditTime: 2023-09-19 10:04:22
 * Copyright: 2023 RJGF CO.,LTD. All Rights Reserved.
 * @Descripttion: 
 */
import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomeView,
    meta: {
      title: 'Run3D'
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.title){
    document.title = to.meta.title
  }
  next()
})

export default router
