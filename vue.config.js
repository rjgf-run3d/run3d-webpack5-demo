/*
 * @Author: sky
 * @Date: 2023-09-13 00:17:51
 * @LastEditors: sky
 * @LastEditTime: 2024-03-04 18:29:33
 * Copyright: 2023 RJGF CO.,LTD. All Rights Reserved.
 * @Descripttion:
 */
const { defineConfig } = require("@vue/cli-service");
const Run3DSource = "node_modules/@rjgf/run3d-engine/Source";
const Run3DWorkers = "../Build/Cesium/Workers";
const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");
const webpack = require("webpack");

module.exports = defineConfig({
  transpileDependencies: true,
  productionSourceMap: false,
  publicPath:
    process.env.NODE_ENV === "production" ? "/run3d-webpack5-demo/" : "/",
  configureWebpack: {
    devtool: "source-map",
    output: {
      sourcePrefix: "",
    },
    resolve: {
      fallback: { https: false, zlib: false, http: false, url: false },
      mainFiles: ["index", "Cesium"],
    },
    plugins: [
      new CopyWebpackPlugin({
        patterns: [
          { from: path.join(Run3DSource, Run3DWorkers), to: "Workers" },
          { from: path.join(Run3DSource, "Assets"), to: "Assets" },
          { from: path.join(Run3DSource, "Widgets"), to: "Widgets" },
          { from: path.join(Run3DSource, "ThirdParty"), to: "ThirdParty" },
        ],
      }),
      new webpack.DefinePlugin({
        CESIUM_BASE_URL: JSON.stringify(""),
      }),
    ],
  },
});
